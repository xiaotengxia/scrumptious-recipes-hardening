from django.urls import path

from meal_plans.views import(
    MealPlanListView,
    MealPlanDetailView,
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplans_list"),
    path("<int:pk>", MealPlanDetailView.as_view(), name="mealplan_detail"),
    path("new/", MealPlanCreateView.as_view(), name="mealplan_new"),
    path("<int:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplan_delete"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="mealplan_edit")

]